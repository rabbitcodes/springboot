package com.rabbit.spring.tutorial.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rabbit.spring.tutorial.mail.MailSender;
import com.rabbit.spring.tutorial.mail.MockMailSender;

@RestController
public class MailController {
	@Resource
	private MailSender mailsender;
	
	@RequestMapping("/mail")
	public String sendMail(){
		mailsender.send("abc@insyncinfo.com", "test mail", "This is a test mail. Please ignore.");
		return "Mail sent";
	}
}
