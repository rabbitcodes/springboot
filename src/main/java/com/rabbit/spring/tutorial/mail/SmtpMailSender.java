package com.rabbit.spring.tutorial.mail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SmtpMailSender implements MailSender {

	private static final Log log = LogFactory.getLog(SmtpMailSender.class);

	@Override
	public void send(String to, String subject, String body) {
		log.info("sending SMTP mail to " + to);
		log.info("mail subject: " + subject);
		log.info("mail body: " + body);

	}

}
